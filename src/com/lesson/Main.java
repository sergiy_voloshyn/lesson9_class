package com.lesson;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {


    static AtomicInteger counter = new AtomicInteger();

    public static synchronized void inc() {
        //counter++;

    }

    public static void main(String[] args) {


        // write your code here
        Counter counter1 = new Counter();
        System.out.println("start 1");
        counter1.start();

        System.out.println("start 2");
        Thread thread = new Thread(new Counter2());
        thread.start();


    }


    static class Counter2 implements Runnable {
        @Override
        public void run() {

            // int counter = 0;
            for (int i = 0; i < 1_000_000; i++) {
                counter.incrementAndGet();
                // inc();

            }
            System.out.println(counter);


        }
    }

    static class Counter extends Thread {
        @Override
        public void run() {

            //   int counter = 0;
            for (int i = 0; i < 1_000_000; i++) {
                counter.getAndIncrement();
                //  inc();

            }
            System.out.println(counter);


        }
    }
   /* public void method()
        synchronized (Main.class)
    {

    }*/

}

